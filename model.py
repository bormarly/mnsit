from keras import backend as K
from keras.utils import plot_model
from matplotlib import pyplot as plt
from keras import models, layers, optimizers

from model_parameters import BATCH_SIZE, HIDDEN_UNITS, DROPOUT
from prepare_dataset import input_size, num_labels, x_train_tenzor, x_test_tenzor, y_test, y_train

model = models.Sequential()
model.add(layers.Dense(HIDDEN_UNITS, activation=K.relu, input_dim=input_size))
model.add(layers.Dropout(DROPOUT))
model.add(layers.Dense(HIDDEN_UNITS, activation=K.relu))
model.add(layers.Dropout(DROPOUT))
model.add(layers.Dense(num_labels, activation=K.softmax))

print(model.summary())

plot_model(model, to_file='images/mlt-mnist.png', show_shapes=True)

# loss function for one-hot vector
# use of adam optimizer
# accuracy is a good metric for classification tasks
model.compile(
    loss='categorical_crossentropy',
    optimizer=optimizers.Adam(),
    metrics=['accuracy']
)

history = model.fit(
    x=x_train_tenzor,
    y=y_train,
    epochs=20,
    batch_size=BATCH_SIZE
)

model.save('modelnew.h5')

# training plot
acc = history.history['accuracy']
loss = history.history['loss']

epochs = range(1, len(acc) + 1)
plt.plot(epochs, acc, 'b', label='training acc')
plt.title('Training accuracy')
plt.legend()
plt.figure()
plt.plot(epochs, loss, 'b', label='training loss')
plt.title('Training loss')
plt.legend()
plt.show()

# test model
loss, acc = model.evaluate(x_test_tenzor, y_test, batch_size=BATCH_SIZE)
print(f'Test accuracy: {acc * 100.0} %')

