import numpy as np
from keras.datasets import mnist
from keras.utils import to_categorical
from matplotlib import pyplot as plt


def prepare_tensor(tensor: np.ndarray, shape: int) -> np.ndarray:
    tensor = np.reshape(tensor, [-1, shape])
    tensor = tensor.astype(np.float32) / 255
    return tensor


# set type of variable
x_train: np.ndarray
x_train: np.ndarray
x_test: np.ndarray
y_test: np.ndarray

(x_train, y_train), (x_test, y_test) = mnist.load_data()

unique, counts = np.unique(y_train, return_counts=True)
print('Train labels:')
print('\n'.join(f'{u}\t{count}' for u, count in zip(unique, counts)))

unique, counts = np.unique(y_test, return_counts=True)
print('Test labels:')
print('\n'.join(f'{u}\t{count}' for u, count in zip(unique, counts)))


def get_random_images(count: int):
    indexes = np.random.randint(0, x_train.shape[0], size=count)
    images = x_train[indexes]
    return images


def show_samples(count: int, images=None):
    # show samples
    total_count = count * count
    images = get_random_images(total_count) if images is None else images

    plt.figure(figsize=(count, count))
    for i, image in enumerate(images):
        plt.subplot(count, count, i + 1)
        plt.imshow(image, cmap='BuPu')
        plt.axis('off')

    plt.show()
    plt.close('all')


if __name__ == '__main__':
    show_samples(5)


# compute the number of labels
num_labels = len(np.unique(y_train))

# convert to one-hot vector
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)

# image dimensions (assumed square)
image_size = x_train.shape[1]
input_size = image_size ** 2

# resize and normalize
x_train_tenzor = prepare_tensor(x_train, input_size)
x_test_tenzor = prepare_tensor(x_test, input_size)
