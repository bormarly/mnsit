import numpy as np
from keras import models

from prepare_dataset import prepare_tensor, input_size, get_random_images, show_samples


# load model
model: models.Sequential = models.load_model('model.h5')


# create test samples
count_samples = 5
test_images = get_random_images(count_samples ** 2)
show_samples(count_samples, test_images)
tenzor = prepare_tensor(test_images, input_size)

# predict test samples
result = model.predict_classes(tenzor)

print('Result: ')
print(np.array(result).reshape(count_samples, count_samples))

